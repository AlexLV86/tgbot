package main

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/AlexLV86/tgbot/internal/api"
	"gitlab.com/AlexLV86/tgbot/internal/telegram"
)

func main() {
	botAPI, err := tgbotapi.NewBotAPI(Token)
	if err != nil {
		panic(err)
	}
	subscribersManager := telegram.NewSubscribersManager()
	telegramBot := &telegram.TelegramBot{API: botAPI, Subscribers: subscribersManager}

	// запуск горутины для получения обновлений
	go telegramBot.Fetch()
	handler := api.NewHandler(telegramBot, subscribersManager)

	// Запуск REST API на порту 8080
	api.StartServer(handler, "8080")

}
