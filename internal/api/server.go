package api

import "github.com/gin-gonic/gin"

func StartServer(handler *Handler, port string) {
	r := gin.Default()
	handler.RegisterRoutes(r)
	r.Run(":" + port)
}
