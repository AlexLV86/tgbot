package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/AlexLV86/tgbot/internal/telegram"
)

type Handler struct {
	sender      telegram.BotSender
	subscribers *telegram.SubscribersManager
}

func NewHandler(sender telegram.BotSender, subs *telegram.SubscribersManager) *Handler {
	return &Handler{
		sender:      sender,
		subscribers: subs,
	}
}

func (h *Handler) RegisterRoutes(r *gin.Engine) {
	r.POST("/send", h.sendBroadcastMessage)
}

func (h *Handler) sendBroadcastMessage(c *gin.Context) {
	message := c.PostForm("message")
	if message == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Сообщение не может быть пустым"})
		return
	}
	// как будто бы лучше перевести этот метод в отдельный сервис, а не оставлять в хендлере
	err := h.BroadcastMessageToAllSubscribers(message)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Ошибка при отправке сообщения"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Сообщение отправлено!"})
}

// перенесите этот метод в отдельный сервис
func (h *Handler) BroadcastMessageToAllSubscribers(message string) error {
	for _, chatID := range h.subscribers.GetAllSubscribers() {
		err := h.sender.Send(chatID, message)
		if err != nil {
			return err
		}
	}
	return nil
}
