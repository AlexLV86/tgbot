package store

import "time"

type Review struct {
	ID        int64
	ClientID  int64
	MasterID  int64
	Rating    int // оценка от 1 до 5
	Comment   string
	CreatedAt time.Time
}

type ReviewRepository interface {
	// Create создает новый отзыв в базе данных.
	Create(review Review) (int64, error)
	// Update обновляет отзыв в базе данных.
	Update(review Review) error
	// Delete удаляет отзыв из базы данных.
	Delete(id int64) error
	// GetByID получает отзыв по его ID.
	GetByID(id int64) (*Review, error)
	// GetByMasterID получает все отзывы, связанные с данным мастером.
	GetByMasterID(masterID int64) ([]Review, error)
	// GetByClientID получает все отзывы, оставленные данным клиентом.
	GetByClientID(clientID int64) ([]Review, error)
}
