package store

type UserRepository interface {
	// Create создает нового пользователя (мастера или клиента) в базе данных.
	Create(user User) (int64, error)
	// Update обновляет данные пользователя в базе данных.
	Update(user User) error
	// Delete удаляет пользователя из базы данных.
	Delete(id int64) error
	// GetByID получает пользователя по его ID.
	GetByID(id int64) (*User, error)
}

type User struct {
	ID       int64
	Name     string
	Email    string
	Role     UserRole // "client", "master" или обе роли
	Password string   // хэшированный пароль
}

type UserRole uint8

const (
	Client UserRole = iota
	Master
	Both
)
