package store

import "time"

type Appointment struct {
	ID        int64
	ClientID  int64
	MasterID  int64
	ServiceID int64 // Идентификатор выбранной услуги
	CreatedAt time.Time
	Time      time.Time // Время, когда клиент записан на услугу
}

type AppointmentRepository interface {
	// Create создает новую запись в базе данных.
	Create(appointment Appointment) (int64, error)
	// Update обновляет запись в базе данных.
	Update(appointment Appointment) error
	// Delete удаляет запись из базе данных.
	Delete(id int64) error
	// GetByID получает запись по её ID.
	GetByID(id int64) (*Appointment, error)
	// GetByClientID получает все записи, связанные с данным клиентом.
	GetByClientID(clientID int64) ([]Appointment, error)
	// GetByMasterID получает все записи, связанные с данным мастером.
	GetByMasterID(masterID int64) ([]Appointment, error)
	// GetByServiceID получает все записи, связанные с определенной услугой.
	GetByServiceID(serviceID int64) ([]Appointment, error)
}
