package store

import "time"

type Service struct {
	ID          int64
	MasterID    int64         // Идентификатор мастера, предоставляющего услугу
	Name        string        // Название услуги, например "Стрижка" или "Маникюр"
	Duration    time.Duration // Продолжительность выполнения услуги
	Price       float64       // Стоимость услуги
	Description string        // Описание услуги
	CreatedAt   time.Time     // Дата создания услуги в системе
	UpdatedAt   time.Time     // Дата последнего обновления услуги
}

type ServiceRepository interface {
	// Create создает новую услугу в базе данных.
	Create(service Service) (int64, error)
	// Update обновляет данные услуги в базе данных.
	Update(service Service) error
	// Delete удаляет услугу из базы данных.
	Delete(id int64) error
	// GetByID получает услугу по её ID.
	GetByID(id int64) (*Service, error)
	// GetByMasterID получает все услуги, предоставляемые данным мастером.
	GetByMasterID(masterID int64) ([]Service, error)
}
