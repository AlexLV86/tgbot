package postgre

import (
	"context"
	"log"

	"gitlab.com/AlexLV86/tgbot/internal/models"
)

func AddMaster(master models.Master) (int64, error) {
	const query = `
		INSERT INTO masters (name, specialty, created_at, updated_at) 
		VALUES ($1, $2, $3, $4) RETURNING id
	`
	var id int64
	err := pool.QueryRow(
		context.Background(),
		query,
		master.Name, master.Specialty, master.CreatedAt, master.UpdatedAt,
	).Scan(&id)
	if err != nil {
		log.Printf("Failed to insert master: %v", err)
		return 0, err
	}
	return id, nil
}

func GetMasterByID(id int64) (*models.Master, error) {
	const query = `
		SELECT id, name, specialty, created_at, updated_at 
		FROM masters WHERE id = $1
	`
	m := &models.Master{}
	err := pool.QueryRow(context.Background(), query, id).
		Scan(&m.ID, &m.Name, &m.Specialty, &m.CreatedAt, &m.UpdatedAt)
	if err != nil {
		log.Printf("Failed to get master by id %d: %v", id, err)
		return nil, err
	}
	return m, nil
}

// ... другие функции запросов для Master
