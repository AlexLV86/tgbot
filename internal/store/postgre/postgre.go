package postgre

import (
	"context"
	"log"

	"github.com/jackc/pgx/v4/pgxpool"
)

var pool *pgxpool.Pool

func InitDB(connectionString string) error {
	var err error
	pool, err = pgxpool.Connect(context.Background(), connectionString)
	if err != nil {
		log.Fatalf("Unable to connect to database: %v\n", err)
		return err
	}
	return nil
}

// Close will close the database connection
func Close() {
	pool.Close()
}
