package store

import "time"

// наверно WorkDay и ExceptionDay лучше добавить привязку сразу к мастеру как шаблоны
type WorkDay struct {
	Day       int // день недели, 0 - воскресенье, 1 - понедельник и т.д.
	StartTime time.Time
	EndTime   time.Time
}

type ExceptionDay struct {
	Date      time.Time
	StartTime *time.Time // может быть nil, если мастер не работает в этот день
	EndTime   *time.Time // аналогично
}

type MasterSchedule struct {
	MasterID      int64
	StandardDays  []WorkDay      // Стандартный рабочий график
	ExceptionDays []ExceptionDay // Исключения из стандартного графика
}

type ScheduleEntry struct {
	ID        int64
	MasterID  int64     // Связь с конкретным мастером
	StartTime time.Time // Начало рабочего времени
	EndTime   time.Time // Конец рабочего времени
	IsWorking bool      // Рабочий ли это день (или выходной)
}

type ScheduleRepository interface {
	// GetScheduleByMaster получает расписание для конкретного мастера.
	GetScheduleByMaster(masterID int64) (MasterSchedule, error)
	// UpdateSchedule обновляет расписание для конкретного мастера.
	UpdateSchedule(masterID int64, entries []ScheduleEntry) error
}
