package store

type Relation struct {
	ClientID int64
	MasterID int64
}

type RelationsRepository interface {
	// Create создает новое отношение между клиентом и мастером в базе данных.
	Create(relation Relation) (int64, error)
	// Delete удаляет отношение из базы данных.
	Delete(clientID int64, masterID int64) error
	// GetByClientID получает всех мастеров, связанных с данным клиентом.
	GetMastersByClient(clientID int64) ([]User, error)
	// GetClientsByMaster получает всех клиентов, связанных с данным мастером.
	GetClientsByMaster(masterID int64) ([]User, error)
}
