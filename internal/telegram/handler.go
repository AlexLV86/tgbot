package telegram

import (
	"fmt"
	"log"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

type BotSender interface {
	Send(chatID int64, message string) error
}

type TelegramBot struct {
	API         *tgbotapi.BotAPI
	Subscribers *SubscribersManager
}

func (t *TelegramBot) Send(chatID int64, message string) error {
	msg := tgbotapi.NewMessage(chatID, message)
	_, err := t.API.Send(msg)
	return err
}

func (t *TelegramBot) Fetch() {
	// проверить это смещение после перезапуска приложения
	// получим все обновления сначала или с последнего
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := t.API.GetUpdatesChan(u)
	if err != nil {
		log.Fatal(err)
		return
	}

	for update := range updates {
		// обработка обновления
		user := update.Message.From

		log.Printf("User ID: %d, First Name: %s, Last Name: %s, Username: @%s, Language Code: %s",
			user.ID,
			user.FirstName,
			user.LastName,
			user.UserName,
			user.LanguageCode)
		log.Printf("Chat ID: %d", update.Message.Chat.ID)
		if update.Message != nil && strings.HasPrefix(update.Message.Text, "/start") {
			master := strings.TrimPrefix(update.Message.Text, "/start")
			// вот здесь и нужно проверять есть ли уже этот пользователь у этого мастера и если есть, то ответить ему
			// рассказав на что он записан или что он может записаться к этому мастеру
			if t.Subscribers.AddSubscriber(update.Message.Chat.ID) {
				t.Send(update.Message.Chat.ID, fmt.Sprintf("Привет, %s! Я %s.", user.FirstName, master))
			} else {
				t.Send(update.Message.Chat.ID, fmt.Sprintf("Привет, %s! Я \"%s\". Ты уже записан ко мне, буду ждать тебя.",
					user.FirstName, master))
			}
			// if update.Message != nil && update.Message.Text == "/start master_A" {
			// 	// Клиент пришел к мастеру А
			// 	t.Send(update.Message.Chat.ID, "Привет! Я master_A.")
			// }
		}
		// Greeting for new users
		if update.Message.NewChatMembers != nil && len(*update.Message.NewChatMembers) > 0 {
			for _, newUser := range *update.Message.NewChatMembers {
				if newUser.ID == t.API.Self.ID {
					t.Send(update.Message.Chat.ID, "Привет! Я ваш новый бот.")
				}
			}
		}

		if update.Message.Text == "помощь" {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Чем могу помочь?")
			t.API.Send(msg)
		}
		if update.Message != nil && update.Message.Text == "/hello" {
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Привет! Я твой новый бот!")
			t.API.Send(msg)
		}
		// update.UpdateID++
	}

}
