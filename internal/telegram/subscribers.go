package telegram

import "sync"

// здесь реализовано хранение и управление подписчиками

type SubscribersManager struct {
	subscribers map[int64]struct{}
	mux         sync.RWMutex
}

func NewSubscribersManager() *SubscribersManager {
	return &SubscribersManager{
		subscribers: make(map[int64]struct{}),
	}
}

func (s *SubscribersManager) AddSubscriber(chatID int64) bool {
	s.mux.Lock()
	defer s.mux.Unlock()
	if _, ok := s.subscribers[chatID]; ok {
		return false
	}
	s.subscribers[chatID] = struct{}{}
	return true
}

func (s *SubscribersManager) GetAllSubscribers() []int64 {
	s.mux.RLock()
	defer s.mux.RUnlock()

	var chats []int64
	for chat := range s.subscribers {
		chats = append(chats, chat)
	}
	return chats
}
