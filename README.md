store/
│
├── user_store.go        # Интерфейс для работы с пользователями (клиентами и мастерами)
├── appointment_store.go # Интерфейс для работы с записями
├── review_store.go      # Интерфейс для работы с отзывами
└── relations_store.go   # Интерфейс для работы с отношениями между клиентами и мастерами
│
├── models/                   # Модели данных
│   ├── user.go               # Модель пользователя (может быть клиентом или мастером)
│   ├── appointment.go       # Модель записи
│   ├── review.go            # Модель отзыва
│   └── relation.go          # Модель отношения между клиентом и мастером
│
├── postgre/                  # Реализации интерфейсов для PostgreSQL
│   ├── user.go               # Реализация user_store.go для PostgreSQL
│   ├── appointment.go        # Реализация appointment_store.go для PostgreSQL
│   ├── review.go             # Реализация review_store.go для PostgreSQL
│   └── relation.go           # Реализация relations_store.go для PostgreSQL
